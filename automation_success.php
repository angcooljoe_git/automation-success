<?php
/**
 * @package Automation Success
 * @version 1
 */
/*
Plugin Name: Automation Success
Plugin URI: 
Description: 
Author: 
Version: 1
Author URI: 
*/
#error_reporting(E_ALL);
#ini_set("display_errors",1);

require_once "lib/infusionsoft/src/isdk.php";
define("CI_APP_NAME",get_option("_automation_success_app_name"));
define("CI_API_KEY",get_option("_automation_success_api_key"));

spl_autoload_register(function ($class_name) {
    if(file_exists(WP_PLUGIN_DIR."/automation_success/model/".strtolower($class_name). ".php")):
        require WP_PLUGIN_DIR."/automation_success/model/".strtolower($class_name). ".php";
    endif;
    if(file_exists(WP_PLUGIN_DIR."/automation_success/utilities/".strtolower($class_name). ".php")):
        require WP_PLUGIN_DIR."/automation_success/utilities/".strtolower($class_name). ".php";
    endif;
});

$iSDK = new iSDK;
class AUTOSUCCESS{
    public static function iSDK(){
        global $iSDK;
        $iSDK->cfgCon("connectionName");
        return $iSDK;
    }

    public static function wpdb(){
        global $wpdb;
        return $wpdb;
    }

    function create_db(){
        
        $sql = "
        CREATE TABLE `automationsuccess_proofs` (
            `id` int(11) NOT NULL,
            `tag_id` int(11) NOT NULL,
            `tag_name` varchar(250) NOT NULL,
            `message` text NOT NULL,
            `image` text NOT NULL,
            `before_notif_show` int(11) NOT NULL,
            `delay_next` int(11) NOT NULL
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ";
        $response = self::wpdb()->query($sql);
      
        $sql = "ALTER TABLE `automationsuccess_proofs`
        ADD PRIMARY KEY (`id`);";
        self::wpdb()->query($sql);

        $sql = "ALTER TABLE `automationsuccess_proofs`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";
        self::wpdb()->query($sql);
    } //create_db
    

} // AUTOSUCCESS_CI

function auto_success_view($view,$data=array() ){
    extract($data);
    ob_start();
        require "view/v_".$view.".php";
        $out = ob_get_contents();
    ob_end_clean();
    
    if($is_var) return $out;
    else echo $out;
}


function auto_success_ctrl(){
    $controller = "c_main";
  
    if(!empty($_REQUEST["c"]) ):
        $controller = "c_".$_REQUEST["c"];
    endif;

    if(!empty($_REQUEST["task"]) ):
        $task = $_REQUEST["task"];
    else:
        $task = "index";
    endif;
    
    require "controller/".$controller.".php";
    new $controller;
    $controller::$task();
 
} // auto_success_ci_ctrl

function auto_success_snippet(){
    require_once("assets/snippet.php");
    die();
} // auto_success_snippet

add_action("wp_ajax_auto_success_ajax", "auto_success_ctrl");

if(isset($_GET["task"]) && $_GET["task"] == "receive") add_action("wp_ajax_nopriv_auto_success_ajax", "auto_success_ctrl");

add_action("wp_ajax_nopriv_auto_success_snippet", "auto_success_snippet");
add_action("wp_ajax_auto_success_snippet", "auto_success_snippet");


function autosuccess_outjson($d){
    echo json_encode($d);
}

function automation_success( $atts ){
    add_menu_page( 'automation_success', 'Automation Success', 'manage_options', 'automation_success', 'auto_success_ctrl', "" ); 
    
    add_submenu_page( 'automation_success', 'Proof', 'Proof', 'manage_options', '?page=automation_success&task=proof');
    add_submenu_page( 'automation_success', 'Date Format', 'Date Format', 'manage_options', '?page=automation_success&c=dateformat&task=date_format');
    add_submenu_page( 'automation_success', 'Date Calculator', 'Date Calculator', 'manage_options', '?page=automation_success&c=datecalculator');
    add_submenu_page( 'automation_success', 'Math Calculator', 'Math Calculator', 'manage_options', '?page=automation_success&c=math_calculator');

    add_submenu_page( 'automation_success', 'Settings', 'Settings', 'manage_options', '?page=automation_success&task=settings');

}//auto_success_contact_import

add_action('admin_menu', 'automation_success');


function my_media_lib_uploader_enqueue() {
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts', 'my_media_lib_uploader_enqueue');


register_activation_hook( __FILE__, array( 'AUTOSUCCESS', 'create_db' ) );



 