<?php 
class M_CI_INFUSIONSOFT extends AUTOSUCCESS{
    public static function sess_csv_to_array(){
        $target_file = $_SESSION["_ci_target_file"];

        # detect delimiter
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
    
        $handle = fopen( $target_file , "r");
        $firstLine = fgets($handle);
        fclose($handle); 
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }
    
        $delimeter = array_search(max($delimiters), $delimiters);

        $rows = array();
        if (($handle = fopen( $target_file , "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, 1000, $delimeter  )) !== FALSE) {
                $new_data = array();
                
                $rows[]   = $data;
                
                $i++;
            }
            fclose($handle);
        }
        return $rows;
    } // sess_csv_to_array

    public static function get_standard_is_fields(){
        $fields = "AccountId, Address1Type, Address2Street1, Address2Street2, Address2Type, Address3Street1, Address3Street2, Address3Type, Anniversary, AssistantName, AssistantPhone, BillingInformation, Birthday, City, City2, City3, Company, CompanyID, ContactNotes, ContactType, Country, Country2, Country3, CreatedBy, DateCreated, Email, EmailAddress2, EmailAddress3, Fax1, Fax1Type, Fax2, Fax2Type, FirstName, Groups, Id, JobTitle, LastName, LastUpdated, LastUpdatedBy, LeadSourceId, Leadsource, MiddleName, Nickname, OwnerID, Password, Phone1, Phone1Ext, Phone1Type, Phone2, Phone2Ext, Phone2Type, Phone3, Phone3Ext, Phone3Type, Phone4, Phone4Ext, Phone4Type, Phone5, Phone5Ext, Phone5Type, PostalCode, PostalCode2, PostalCode3, ReferralCode, SpouseName, State, State2, State3. StreetAddress1, StreetAddress2, Suffix, Title, Username, Validated, Website, ZipFour1, ZipFour2, ZipFour3";
        $all_fields = explode(",",$fields);
        

        return $all_fields;
    } // get_standard_is_contact_fields

    public static function get_custom_is_fields(){
        $table = 'DataFormField';
        $limit = 1000;
        $page = 0;
        $queryData = array('FormId' => "-1" );
        $selectedFields = array("Id","Name");
        $orderBy = '';
        $ascending = true;

        $custom_fields = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
        
        return $custom_fields;   
    } // get_custom_date

    public static function get_date_format(){
        $date_formats   = array();
        $date_formats[] = "l F jS at g:ia "; 
        $date_formats[] = "l F jS, Y, g:ia ";
        $date_formats[] = "l F j, Y"; 
        $date_formats[] = "l, F jS"; 
        $date_formats[] = "F j, Y g:ia ";
        $date_formats[] = "m/d/y g:ia ";
        $date_formats[] = "m/d/y";
        $date_formats[] = "d/m/y g:ia ";
        $date_formats[] = "d/m/y";
        $date_formats[] = "F Y";
        $date_formats[] = "F";
        $date_formats[] = "l";
        $date_formats[] = "custom";
        return $date_formats;
    } // get_date_format
}
?>