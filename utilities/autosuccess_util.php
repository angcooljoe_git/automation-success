<?php 
class autosuccess_util extends AUTOSUCCESS{

    public static function infusionSoftFieldsSelect2($data){
        $custom_fields = M_CI_INFUSIONSOFT::get_custom_is_fields();
        $fields        = M_CI_INFUSIONSOFT::get_standard_is_fields();
        $default       = "";
        if(!empty($data["default"])) $default = $data["default"];
        ?>
        <select <?php if(!empty($data["required"])) echo "required"; ?> data-id="<?php echo $data["id"];?>" 
                class="jq-custom-select <?php if(!empty($data["class"])) echo $data["class"];?>" 
                data-field="<?php echo $data["field"];?>" 
                style="width:100%" 
                name="<?php echo $data["name"]?>" >
            <option></option>
            <optgroup label="Standard Fields">
            <?php 
                foreach($fields as $efield):
                    if($efield==$default) $selected = " selected ";
                    else $selected = "  ";
                    ?> 
                    <option <?php echo $selected;?> label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                    <?php 
                endforeach;
                ?>
            </optgroup>
            <optgroup label="Custom Fields">
            <?php 
            foreach($custom_fields as $efield):
                if("_".$efield["Name"]==$default) $selected = " selected ";
                else $selected = "  ";
                ?> 
                <option <?php echo $selected;?>  label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
                <?php 
            endforeach;
            ?>
            </optgroup>

        </select>
        <?php
    }

}
?>