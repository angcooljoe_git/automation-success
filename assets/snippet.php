<?php 
if(empty(CI_APP_NAME)) die();
if(empty($_REQUEST["id"])) die();

$proof_details         = AUTOSUCCESS::wpdb()->get_results("SELECT * FROM automationsuccess_proofs WHERE id=".$_REQUEST["id"]);
$proof_details         = $proof_details[0];

$img        = $proof_details->image;
$group_name = $proof_details->tag_id;
$message    = "<div style='padding-top:10px;font-size:13px; max-width:250px;line-height:20px'>".$proof_details->message."</div>";
$sec_before = $proof_details->before_notif_show;
$sec_before = $sec_before * 1000;

$sec_next   = $proof_details->delay_next;
$sec_next   = $sec_next * 1000;


if(!empty($img)) $img = "<div style='background:url(".$img."); background-size:cover; background-repeat:no-repeat; border-radius:50px; width:100px; height:100px;' > </div>";

$table          = 'Contact';
$limit          = 50;
$page           = 0;
$queryData      = array('Groups' => $group_name);
$selectedFields = array('Id','FirstName',"LastName");
$orderBy        = 'FirstName';
$ascending      = true;

$contacts     = AUTOSUCCESS::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
header('Content-Type: application/javascript');
ob_start();
echo file_get_contents("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js");



?>
jQuery(document).ready(function($){
    var contacts = <?php echo json_encode($contacts);?>;
    var i = 0;
    total = contacts.length;

    cardDisplay = function(i,contacts){
        card = "<div style='display:table;width:100%'>";
        card += "<div style='display:table-row;'>";
        card += "<div style='display:table-cell;'><?php echo $img;?></div>";
        card += "<div style='display:table-cell; vertical-align:top; padding:10px 20px 0px 20px; font-family: arial; color:#999'><div style='color:#333;font-size:16px;'>"+contacts[i].FirstName+" "+contacts[i].LastName+"</div><?php echo $message;?></div>";
        card += "</div>";
        $(".automation-floating-container").html(card).slideToggle( "fast" );
        
        setTimeout(function(){ 
            $(".automation-floating-container").slideToggle();
            
        },<?php echo $sec_next - 1000 ;?>);
    }
    

    setTimeout(function(){
        $("body").append("<div class='automation-floating-container' style='z-index:9999;position:fixed; bottom:0px; left:0px; border-radius:5px; border:1px solid #efefef;padding:10px;-webkit-box-shadow: 0px 2px 5px -2px rgba(0,0,0,0.46); -moz-box-shadow: 0px 2px 5px -2px rgba(0,0,0,0.46); box-shadow: 0px 2px 5px -2px rgba(0,0,0,0.46); background:#fff;'></div>");
        $(".automation-floating-container").slideToggle();
        cardDisplay(i,contacts);
        contactLoop = setInterval(function(){
            i++;
            cardDisplay(i,contacts);
            if(i==total){
                //clearInterval(contactLoop);
                i = 0;
            }
        },<?php echo $sec_next;?>);

    },<?php echo $sec_before;?>);

});
<?php
$snippet = ob_get_contents();
ob_end_clean();
echo trim($snippet);
/*
<div >
    image --- Name, From State
              Mesasge
</div>
*/
?>