
<style type="text/css">
.form-group-row{
    margin-bottom:10px;
}
#poststuff .postbox .inside{
    padding:10px;
}

.form-group{
    display:table;
    margin-bottom:5px;
    width:100%
}
.form-group input[type=text]{
    width:300px;
    height:30px;
}
.form-group .row{
    display:table-row;
}
.form-group .row .col{
    display:table-cell;
    padding:5px 10px;
}
.form-group .row .col:first-child{
    width:150px;
    text-align:right;
}
.list-group{
    border-radius:5px;
    background:#fff;
    padding:5px 10px 10px 10px;
    border:1px solid #ccc;
    margin-top:0px;
    width:279px;
}
.list-group .list-group-item{
    border-bottom:1px solid #efefef;
    padding:5px 3px;
    clear:both;
    cursor:pointer;
    background:#fff;
}
.ci-dropdown-container{
    position: absolute;
    width: 100%;
}

.date-calculator .col{
    vertical-align:top;
}
.date-calculator .select2-container .select2-selection--single{
    height:32px !important;
}
.col-label{
    vertical-align:middle !important;
    font-weight:bold;
}

.with-label{
    background:#efefef;
    padding:0px 2px 0px 0px;
    display:table;
    border-radius:4px;
}
.with-label .form-control{
    display:table-cell;
    width:100px !important;
}
.with-label .label{
    display:table-cell;
    text-align:center;
    padding:0px 10px;
}

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover{
    border: 1px solid #0071ad !important;
    background: #0071ad !important;
    font-weight: normal;
    color: #ffffff;
    border-top:none !important;
    border-right:none !important;
    border-left:none !important;
}
.ui-widget-header{
    background:none !important;
}
td{
    position:relative
}
.align-left{
    text-align:left !important;
}
</style>


<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css"/>

<script type="text/javascript">
var AutoSuccess;
var searchGroupXHR;
jQuery(document).ready(function($){
    $(".jq-custom-select").select2();
    $("#tabs").tabs();

    AutoSuccess = {

        deleteMathCalculator : function(e){
            r = confirm("Are you sure you want to delete this item?");
            if(r==true){
                $.ajax({
                    url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=math_calculator&task=delete",
                    type : "post",
                    data : {
                        id : $(e).data("id")
                    },
                    beforeSend : function(e){
                        $(e).parent().parent().fadeTo("fast",.5);
                    },
                    success : function(d){
                        $(e).parent().parent().fadeOut().remove();
                    }
                });
            }
        }, // deleteMathCalculator

        deleteDateCalculator : function(e){
            r = confirm("Are you sure you want to delete this item?");
            if(r==true){
                $.ajax({
                    url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=datecalculator&task=delete",
                    type : "post",
                    data : {
                        id : $(e).data("id")
                    },
                    beforeSend : function(e){
                        $(e).parent().parent().fadeTo("fast",.5);
                    },
                    success : function(d){
                        $(e).parent().parent().fadeOut().remove();
                    }
                });
            }
        }, // deleteDateCalculator
        
        listMathCalculator : function(){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=math_calculator&task=list_mathcalculator",
                type : "post",
                beforeSend : function(e){
                   
                },
                success : function(d){
                    $(".div-math-calculators-list").html(d);
                    $(".jq-custom-select").select2();
                }
            });
        }, // listMathCalc
        
        listDateCalculator : function(){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=datecalculator&task=list_datecalculator",
                type : "post",
                beforeSend : function(e){
                   
                },
                success : function(d){
                    $(".div-calculators-list").html(d);
                    $(".jq-custom-select").select2();
                }
            });
        },

        saveNewMathCalculator : function(e){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=math_calculator&task=save_new_mathcalculator",
                type : "post",
                data : $(e).serialize(),
                beforeSend : function(e){
                    $(e).find("button").fadeTo("fast",.5);
                },
                success : function(){
                    $(e).find("button").fadeTo("fast",1).html("New Calculator Saved");
                    AutoSuccess.listMathCalculator();
                    $(e).find("select,input[type=number],input[type=text],textarea").val("");
                    setTimeout(function(){
                        $(e).find("button").html("Save");
                    },5000);
                }
            });
        }, //saveNewMathCalculator;

        saveNewDateCalculator : function(e){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=datecalculator&task=save_new_datecalculator",
                type : "post",
                data : $(e).serialize(),
                beforeSend : function(e){
                    $(e).find("button").fadeTo("fast",.5);
                },
                success : function(){
                    $(e).find("button").fadeTo("fast",1).html("New Calculator Saved");
                    AutoSuccess.listDateCalculator();
                    $(e).find("select,input[type=number],textarea, input[type=text]").val("");
                    setTimeout(function(){
                        $(e).find("button").html("Save");
                    },5000);
                }
            });
        }, //saveNewDatecalculator

        submitSettings : function(e){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=settings&task=update_settings",
                type : "post",
                data : $(e).serialize(),
                beforeSend : function(e){
                    $(e).find("button").fadeTo("fast",.5);
                },
                success : function(){
                    $(e).find("button").fadeTo("fast",1).html("Updates saved!");
                }
            });

        }, // submisettings
        saveProof : function(e){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=proof&task=update_proof",
                type : "post",
                data : $(e).serialize(),
                beforeSend : function(e){
                    $(e).find(".button-primary").fadeTo("fast",.5);
                },
                dataType : "json",
                success : function(d){
                    $(e).find(".button-primary").fadeTo("fast",1).html("Updates saved!");
                    if(d.redirect == 1 ){
                        window.location = "?page=automation_success&task=proof_form&is_new&id="+d.id;
                    }
                }
            });
        } // saveProof
    } // AutoSuccess

    $(".txt-search-tag").keyup(function(){
        e = this;
        if(searchGroupXHR != null ){
            searchGroupXHR.abort();
        }

        searchGroupXHR = $.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=main&task=get_infusionsoft_group",
            data : {
                skey : $(e).val(),
            },
            beforeSend : function(){
                $(".ci-dropdown-container").html("<img style='position: absolute;top: -25px;left: 270px;' src='/wp-admin/images/wpspin_light.gif'/>");
            },
            success : function(d){
                $(".ci-dropdown-container").html(d);
            }
        });
    }); //$(".txt-search-tag").keyup(function(){
    
    copyToClipboardURL = function(e){
        var copyText = $("#http-post-url-"+$(e).data("id"));
        copyText.focus();
        copyText.select();
        /* Copy the text inside the text field */
        try{
            document.execCommand("copy");
            $(e).html("URL copied to clipboard");
        }catch(error){
            console.log(error);
        }
        
    } // copy to clipboard
    

});

jQuery(document).on("click",".ci-source-tag",function(){
    console.log(this);
    id = $(this).data("id");
    name = $(this).html();
    $(".hdn-tag-id").val(id);
    $(".txt-search-tag").val(name);
    $(".ci-dropdown-container").html("");
});

jQuery(document).on("click",".button-delete-proof",function(){
    id = $(this).data("id");
    btn = this;
    r = confirm("Are you sure you want to delete this item?");
    if(r===true){
        $.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=proof&task=delete_proof&id="+id,
            beforeSend : function(){
            },
            success : function(d){
                $(btn).parent().parent().fadeOut().remove();
            }
        });   
    }
});

jQuery(document).on("change", ".listen-date-calc-field",function(){
    id = $(this).data("id");
    field = $(this).data("field");
    val = $(this).val();
    e = this;
    $.ajax({
        url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=datecalculator&task=update",
        data : {
            id : id,
            val : val,
            field : field
        },
        success : function(d){
            id = Math.floor((Math.random() * 1090) + 1);
            $(e).parent().append("<span id='saving-placeholder-"+id+"' style='color:#00b54f;position:absolute;right: 14px;top: 11px;background: #efefef;font-size: 11px;padding: 0px 5px;' class='saving-placeholders'>Saved</span>");
            
            setTimeout(function(){
                $('#saving-placeholder-'+id).fadeOut().remove();
            },5000);
        },
        beforeSend : function(){
            $(".saving-placeholders").fadeOut().remove();
        }
    }); 
    
});

jQuery(document).on("change",".listen-add-minus",function(){
    v = $(this).val();
  
    if(v == "custom"){
        $(".hide-on-custom").hide();
        $(".custom").show();
    }else{
        $(".hide-on-custom").show();
        $(".custom").hide();
    }
    console.log(v);
});

jQuery(document).on("blur",".textarea-custom",function(){
    e = this;
    $.ajax({
        url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=datecalculator&task=sample_custom",
        data : {
            custom : $(e).val()
        },
        beforeSend : function(){

        },
        success : function(d){
            $(".sample-custom").html(d);
        }
    });
});


jQuery(document).on("change",".select-date-format", function(){
    e = this;
    if($(e).val() == "custom"){
        $(".custom-select-date-format").show();
    }else{
        $(".custom-select-date-format").hide();
    }
});


jQuery(document).on("change", ".listen_math_calc_field",function(){
    id = $(this).data("id");
    field = $(this).data("field");
    val = $(this).val();
    e = this;

    id = $(this).data("id");
    field = $(this).data("field");
    val = $(this).val();
    e = this;
    $.ajax({
        url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=math_calculator&task=update",
        data : {
            id : id,
            val : val,
            field : field
        },
        type : "post",
        success : function(d){
            id = Math.floor((Math.random() * 1090) + 1);
            $(e).parent().append("<span id='saving-placeholder-"+id+"' style='color:#00b54f;position:absolute;right: 14px;top: 11px;background: #efefef;font-size: 11px;padding: 0px 5px;' class='saving-placeholders'>Saved</span>");
            
            setTimeout(function(){
                $('#saving-placeholder-'+id).fadeOut().remove();
            },5000);
        },
        beforeSend : function(){
            $(".saving-placeholders").fadeOut().remove();
        }
    }); 
}); //jQuery(document).on("change",

</script>