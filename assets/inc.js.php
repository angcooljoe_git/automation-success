<script type="text/javascript">
    var AutoSuccess;
    var searchGroupXHR;
    jQuery(document).ready(function($){
        $(".jq-custom-select").select2();
        $("#tabs").tabs();

        AutoSuccess = {
            submitSettings : function(e){
                $.ajax({
                    url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=settings&task=update_settings",
                    type : "post",
                    data : $(e).serialize(),
                    beforeSend : function(e){
                        $(e).find("button").fadeTo("fast",.5);
                    },
                    success : function(){
                        $(e).find("button").fadeTo("fast",1).html("Updates saved!");
                    }
                });

            }, // submisettings
            saveProof : function(e){
                $.ajax({
                    url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=proof&task=update_proof",
                    type : "post",
                    data : $(e).serialize(),
                    beforeSend : function(e){
                        $(e).find(".button-primary").fadeTo("fast",.5);
                    },
                    dataType : "json",
                    success : function(d){
                        $(e).find(".button-primary").fadeTo("fast",1).html("Updates saved!");
                        if(d.redirect == 1 ){
                            window.location = "?page=automation_success&task=proof_form&is_new&id="+d.id;
                        }
                    }
                });
            } // saveProof
        } // AutoSuccess

        $(".txt-search-tag").keyup(function(){
            e = this;
            if(searchGroupXHR != null ){
                searchGroupXHR.abort();
            }

            searchGroupXHR = $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=main&task=get_infusionsoft_group",
                data : {
                    skey : $(e).val(),
                },
                beforeSend : function(){
                    $(".ci-dropdown-container").html("<img style='position: absolute;top: -25px;left: 270px;' src='/wp-admin/images/wpspin_light.gif'/>");
                },
                success : function(d){
                    $(".ci-dropdown-container").html(d);
                }
            });
        }); //$(".txt-search-tag").keyup(function(){

        

    });

    jQuery(document).on("click",".ci-source-tag",function(){
        console.log(this);
        id = $(this).data("id");
        name = $(this).html();
        $(".hdn-tag-id").val(id);
        $(".txt-search-tag").val(name);
        $(".ci-dropdown-container").html("");
    });

    jQuery(document).on("click",".button-delete-proof",function(){
        id = $(this).data("id");
        btn = this;
        r = confirm("Are you sure you want to delete this item?");
        if(r===true){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=proof&task=delete_proof&id="+id,
                beforeSend : function(){
                },
                success : function(d){
                    $(btn).parent().parent().fadeOut().remove();
                }
            });   
        }
    });

    </script>