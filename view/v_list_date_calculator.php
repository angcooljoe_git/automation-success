<table class="wp-list-table widefat fixed striped posts">
	<thead>
	    <tr>
            <th>Name</th>
            <th style="width:100px">Start Date</th>
            <th >Then </th>
            <th >Months</th>
            <th >Days</th>
            <th style="width:200px">PHP Strtotime </th>
            <th>Infusionsoft Target Field</th>
            <th >Date Format</th>
            <th> </th>
         </tr>
    </thead>

    <tbody>
        <?php 
        foreach($list as $e):
            ?>
            <tr>
                <td >
                    
                    <input style="width:100% !important; height:35px" type="text" class="form-control listen-date-calc-field" value="<?php echo $e->name;?>" data-id="<?php echo $e->id;?>" data-field="name"/>
                </td>

                <td >
                    <select class="start_date jq-custom-select listen-date-calc-field" data-id="<?php echo $e->id;?>" data-field="start_date">
                        <option value="today" <?php if($e->start_date == "today") echo "selected"; ?> >Today</option>
                        <option value="yesterday" <?php if($e->start_date == "yesterday") echo "selected"; ?>>Yesterday</option>
                        <option value="tomorrow" <?php if($e->start_date == "tomorrow") echo "selected"; ?>>Tomorrow</option>
                    </select>
                </td>
                <td>
                    <select class="jq-custom-select add_minus listen-date-calc-field" style="width:100%" data-id="<?php echo $e->id;?>" data-field="add_minus">
                        <option value="add" <?php if($e->add_minus == "add") echo "selected"; ?>>+ Add</option>
                        <option value="minus" <?php if($e->add_minus == "minus") echo "selected"; ?>>- Minus</option>
                        <option value="minus" <?php if($e->add_minus == "custom") echo "selected"; ?>>Custom</option>
                    </select>
                </td>
                <td>
                    <div class="with-label">
                        <input type="number" class="form-control listen-date-calc-field" value="<?php echo $e->months;?>" data-id="<?php echo $e->id;?>" data-field="months"/>
                        <span class="label">Months</span>
                    </div>
                </td>
                <td>
                    <div class="with-label">
                        <input type="number" class="form-control listen-date-calc-field" value="<?php echo $e->days;?>" data-id="<?php echo $e->id;?>" data-field="days"/>
                        <span class="label">Days</span>
                    </div>
                </td>

                <td>
                    <div class="with-label">
                        <textarea style="width:100% !important; height:50px !important;" class="form-control listen-date-calc-field" data-id="<?php echo $e->id;?>" data-field="custom"><?php echo $e->custom;?></textarea>
                     </div>
                </td>

                <td>
                    <select class="jq-custom-select listen-on-change listen-date-calc-field"  style="width:100%" data-id="<?php echo $e->id;?>" data-field="target_infusionsoft_field">
                        <option></option>
                        <optgroup label="Standard Fields">
                        <?php 
                            $default = $e->target_infusionsoft_field;
                            foreach($fields as $efield):
                                if($efield==$default) $selected = " selected ";
                                else $selected = "  ";
                                ?> 
                                <option <?php echo $selected;?> label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                                <?php 
                            endforeach;
                            ?>
                        </optgroup>
                        <optgroup label="Custom Fields">
                        <?php 
                        foreach($custom_fields as $efield):
                            if("_".$efield["Name"]==$default) $selected = " selected ";
                            else $selected = "  ";
                            ?> 
                            <option <?php echo $selected;?>  label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
                            <?php 
                        endforeach;
                        ?>
                        </optgroup>

                    </select>
                </td>
                
                <td>
                    <select style="width:100%" class="jq-custom-select listen-date-calc-field" data-id="<?php echo $e->id;?>" data-field="date_format">
                        <option></option>
                        <?php 
                        foreach($date_formats as $edate):
                            if($edate==$e->date_format) $selected = " selected ";
                            else $selected = "";

                            if($edate=="custom"): 
                                $date_value = "Custom";
                            else: 
                                $date_value = date( str_replace("at",'\a\t',$edate) );
                            endif;

                        ?>
                        <option value="<?php echo $edate;?>" <?php echo $selected;?> ><?php echo $date_value;?></option>
                        <?php 
                        endforeach;
                        ?>
                    </select>

                    <?php 
                    if($e->date_format == "custom"): 
                        ?> 
                        <div class="custom-select-date-format" style="margin-top:10px">
                            <b>Custom Date Format</b>
                            <input style="width:100%" type="text" value="<?php echo $e->custom_date_format;?>" class="form-control listen-date-calc-field" data-id="<?php echo $e->id;?>" data-field="custom_date_format"  />
                        </div>
                        <?php
                    endif;
                    ?>
                </td>
                
                <td>
                    <?php 
                    $key = md5($e->id);
                    $url = site_url()."/wp-admin/admin-ajax.php?action=auto_success_ajax&c=datecalculator&task=receive&key=".$key;
                    ?>
                    <textarea style="width:100%; opacity:0; width:1px;opacity;position:absolute;" id="http-post-url-<?php echo $e->id;?>"><?php echo $url;?></textarea>
                    <button type="button" class="button button-secondary" onclick="copyToClipboardURL(this)" data-id="<?php echo $e->id;?>">Copy To Clipboard</button>


                    <button type="button" class="button-secondary" onclick="AutoSuccess.deleteDateCalculator(this)" data-id="<?php echo $e->id;?>">Delete</button>
                </td>
            
            </tr>
            <?php
        endforeach;
        ?>
    </tbody>

</table>