<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>
<style type="text/css">
.post-url-gen{
    padding:30px 10px;
}
.post-url-gen a{
    font-size:15px;
}
.date-formatter-list h4{
    font-size:13px;
    text-transform:uppercase;
    margin-top:-5px;
}
.date-formatter-list tr,
.date-formatter-list td,
.date-formatter-list th{
    font-size:12px;
}
.date-formatter-list th{
    font-weight:bolder;
}
</style>

<div id="poststuff" class="date-formatter-list"   >

    <div class="postbox wrap" style="width:20%; float:left; ">

        <button type="button" class="handlediv" aria-expanded="true">
            <span class="screen-reader-text">Toggle panel: Date Format </span>
            <span class="toggle-indicator" aria-hidden="true"></span>
        </button>
       
        <div class="inside">
            <form class="" onsubmit="generateHttpPostURL(this); return false;" >   
            <div class="date-format-content" style="width: ;">
                <h4 style="margin-top:0px">Add New</h4>

                <div class="form-group">
                    <div class="row">
                        Name : 
                        <input type="text" class="form-control" name="name" style="width:100%"/>
                    </div>
                </div>

                <div class="form-group">
                
                    <div class="row">
                             Select Field : 
                            <select class="jq-custom-select" name="update_from_field" style="width:100%">
                                <option></option>
                                <optgroup label="Standard Fields">
                                <?php 
                                    foreach($fields as $efield):
                                        ?> 
                                        <option label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                                        <?php 
                                    endforeach;
                                    ?>
                                </optgroup>
                                <optgroup label="Custom Fields">
                                <?php 
                                foreach($custom_fields as $efield):
                                    ?> 
                                    <option label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
                                    <?php 
                                endforeach;
                                ?>
                                </optgroup>

                            </select>
                     
                    </div> <!-- .row -->
                </div>

                <div class="form-group" style="margin:20px 0px">
                    <div class="row">
                             Select Format :
                            <select style="width:100%" class="jq-custom-select" name="date_format">
                                <option></option>
                                <?php 
                                foreach($date_formats as $edate):
                                    
                                ?>
                                <option value="<?php echo $edate;?>"><?php echo date( str_replace("at",'\a\t',$edate) );?></option>
                                <?php 
                                endforeach;
                                ?>
                            </select>
                     </div> <!-- .row -->
                </div>

                <div class="form-group" >
                    <div class="row">
                            Update Field : 
                            <select style="width:100%" class="jq-custom-select" name="update_to_field">
                                <option></option>
                                <optgroup label="Standard Fields">
                                <?php 
                                    foreach($fields as $efield):
                                        ?> 
                                        <option label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                                        <?php 
                                    endforeach;
                                    ?>
                                </optgroup>
                                <optgroup label="Custom Fields">
                                <?php 
                                foreach($custom_fields as $efield):
                                    ?> 
                                    <option label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
                                    <?php 
                                endforeach;
                                ?>
                                </optgroup>

                            </select>
                     </div> <!-- .row -->
                </div> <!-- .form-group -->

                <div class="form-group" style="margin:20px 0px">
                    <div class="row">
                             <button type="submit" class="button button-primary">Save</button>
                     </div>
                </div> <!-- .form-group -->

            </div> <!-- .date-format-content -->

            

        
        </div> <!-- .inside -->
    
    </div> <!-- .postbox wrap -->

    <div class="postbox wrap" style="float:left; width:75%;  ">
        <div class="inside">                       
            <h4 style="margin-top:0px">List</h4>
            <div class="list-table-content" ></div>
        </div>
    </div>

</div> <!-- #poststuff -->
<div style="clear:both"> </div>

<script type="text/javascript">
jQuery(document).ready(function($) {
 
    dateFormatList = function(){
        $.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=dateformat&task=list",
            type : "post",
            success : function(d){
                $(".list-table-content").html(d);
                setTimeout(function(){
                    $(".jq-custom-select").select2();
                },100);
            },
            beforeSend : function(){
                $(".list-table-content").html("<div style='text-align:center'><img src='<?php echo site_url();?>/wp-includes/images/spinner-2x.gif'/></div>");
            }
        });
    }

    generateHttpPostURL = function(e){
        $.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=dateformat&task=generate_http_post_url",
            type : "post",
            data : $(e).serialize(),
            success : function(d){
                $(e).find("button").fadeTo("fast",1);
                $(e).find("button[type=submit]").html("Saved");
                setTimeout(function(){
                    $(e).find("button[type=submit]").html("Save");
                },5000);
            },
            beforeSend : function(){
                dateFormatList();
                $(e).find("button").fadeTo("fast",.5);
            }
        });
    }

    

    deleteDateFormatList = function(e){
        r = confirm("Are you sure you want to delete this item?");
        id = $(e).data("id");
        if(r === true ){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=dateformat&task=delete_dateformat_list",
                type : "post",
                data : {
                    id : id
                },
                success : function(){
                    $(e).parent().parent().fadeOut().remove();
                }      
            });
        }
    } //

    $(document).on("blur change",".listen-on-change",function(){
        id = $(this).data("id");
        val = $(this).val();
        field = $(this).data("field");
        e = this;
        
        $.ajax({
            url : "<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_ajax&c=dateformat&task=update_date_format",
            type : "post",
            data : {
                id : id,
                val : val,
                field : field
            },
            success : function(d){
                id = Math.floor((Math.random() * 1090) + 1);
                $(e).parent().append("<span id='saving-placeholder-"+id+"' style='color:#00b54f;position:absolute;right: 14px;top: 11px;background: #efefef;font-size: 11px;padding: 0px 5px;' class='saving-placeholders'>Saved</span>");
                
                setTimeout(function(){
                    $('#saving-placeholder-'+id).fadeOut().remove();
                },5000);
            },
            beforeSend : function(){
                $(".saving-placeholders").fadeOut().remove();
            }
        });
        
    });


    setTimeout(function(){
        dateFormatList();
    },100);

});
</script>