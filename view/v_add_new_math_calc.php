<form onsubmit="AutoSuccess.saveNewMathCalculator(this); return false;" style="width:500px;">

    <div class="form-group">
        <div class="row">
            <div class="col label align-left" style="width:50px; font-weight:bold">Name : </div>
            <div class="col align-left">
                <input type="text" class="form-control" name="name" required/>
                
            </div> <!-- col -->

        </div> <!-- .row -->

    </div> <!-- .form-group -->

    <div class="form-group" style="width:500px">
        <div class="row">
            <div class="col" style="width:40%; text-align:left;">
                <h4>Select Infusionsoft Fields</h4>
                <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( 
                    array( "name" => "add_minus_from", 
                           "field" => "add_minus_from",
                           "required" => 1 )
                ); ?>
            </div>
            <div class="col" style="width:20%; text-align:left;">
                <h4> &nbsp; </h4>
                <select class="jq-custom-select add_minus"   name="add_minus" style="width:100%">
                    <option value="add">+</option>
                    <option value="minus">-</option>
                </select>
            </div> <!-- .col -->
            <div class="col" style="width:40%; text-align:left;">
                <h4>Select Infusionsoft Fields</h4>
                <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( array("name"=>"add_minus_to", "field"=>"add_minus_to", "required" => 1 ) ); ?>
            </div> <!-- .col -->
        </div> <!-- .row -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <div class="row">
            <div class="col align-left">
                <h4>Store Result Here </h4>
                <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( array("name"=>"store_result_here", "field"=>"store_result_here", "required" => 1 ) ); ?>
            </div> <!-- .col -->
        </div> <!-- .row -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <div class="row">
            <div class="col align-left">
                <button type="submit" class="button-primary">Save</button>
            </div>
        </div>
    </div> <!-- .form-group -->

</form>