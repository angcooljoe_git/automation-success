
<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>
<div id="poststuff" style="max-width:500px">
    <div class="postbox wrap">
                
        <div class="inside">
            
            <div id="message" class="updated notice notice-success is-dismissible">
                <p > </p>

                <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span>
                </button>

            </div>

            <script type="text/javascript">
                jQuery(document).ready(function($){
                    setTimeout(function(){
                        $.ajax({
                            url : "<?php echo site_url();?>/wp-content/plugins/automation_success/utilities/plugin_updater.php",
                            beforeSend : function(){
                                $("#message p").html("<img style='height:20px; left:14px; position:absolute;' src='<?php echo site_url();?>/wp-includes/js/tinymce/skins/lightgray/img/loader.gif'/> <span style='padding-left:30px'>Checking updates </span>");
                            },
                            success : function(d){
                                $("#message p").html(d);
                            }
                        });

                        <?php 
 
                        $app_name = get_option("_automation_success_app_name");
                   
                        if(empty($app_name)): 
                            ?>
                            window.location = "?page=automation_success&task=settings";
                            <?php
                        endif;
                        ?>
                        
                    },500);
                });
            </script>
       
        </div>
</div>




