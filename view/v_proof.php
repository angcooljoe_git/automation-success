<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>
<div id="poststuff" style="max-width:500px">
    <div class="postbox wrap">
        <button type="button" class="handlediv" aria-expanded="true">
        <span class="screen-reader-text">Toggle panel: Proof</span>
        <span class="toggle-indicator" aria-hidden="true"></span>
        </button>

        <h2 class="hndle ui-sortable-handle">
            <span>Proofs List</span>
            <a href="?page=automation_success&task=proof_form" class="page-title-action" style="float:right; margin-right:-25px">Add New</a>
            
        </h2>
        <div style="clear:both"></div>
       
        <div class="inside">
            
            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th style="width:50px">Tag ID</th>
                        <th>Tag Name</th>
                        <th style="width:100px"> </th>
                        
                    </tr>

                </thead>


                <tbody id="the-list">
                    <?php 
                    foreach($proofs as $eproof):
                    ?>
                    
                        <tr>
                            <td><?php echo $eproof->proof_name;?></td>
                            <td>#<?php echo $eproof->tag_id;?></td>
                            <td><?php echo $eproof->tag_name;?></td>
                            <td style="text-align:right">
                                <a style="line-height: 16px;height:auto;padding: 3px;font-size: 14px;" class="button-primary" href="?page=automation_success&task=proof_form&id=<?php echo $eproof->id;?>"><span class="dashicons dashicons-edit"></span></a>
                                <button style="line-height: 16px;height:auto;padding: 3px;font-size: 14px;" type="button" class="button-secondary button-delete-proof" data-id="<?php echo $eproof->id;?>"><span class="dashicons dashicons-trash"></span></button>
                            </td>
                        </tr>
                    <?php       
                    endforeach;
                    ?>
                </tbody>

            </table>

            
        
        </div> <!-- <div class="inside"> -->

    </div> <!-- .postbox -->

</div> <!-- #poststuff -->




