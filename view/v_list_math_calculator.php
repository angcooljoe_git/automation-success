<table class="wp-list-table widefat fixed striped posts">
	<thead>
	    <tr>
            <th>Name</th>
            <th >From</th>
            <th style="width:100px"> Operation </th>
            <th> To </th>
            <th> Target Infusionsoft Field </th>
            <th style="width:230px"> </th>
         </tr>
    </thead>

    <tbody>
        <?php 
        foreach($list as $e):
            ?>
            <tr> 
                <td style="width:200px"> 
                    <input type="text" class="form-control listen_math_calc_field" data-id="<?php echo $e->id;?>" data-field="name" value="<?php echo $e->name;?>" style="width:100%"/>
                </td>
                <td>
                    <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( 
                        array( "name"        => "add_minus_from", 
                            "field"          => "add_minus_from",
                            "required"       => 1,
                            "class"          => "listen_math_calc_field", 
                            "id"             => $e->id,
                            "default"        => $e->add_minus_from
                        )
                    ); ?>
                </td>
                <td  >
                    <select class="jq-custom-select add_minus listen_math_calc_field"   name="add_minus" style="width:100%" data-id="<?php echo $e->id;?>" data-field="add_minus">
                        <option value="add" <?php if($e->add_minus=="add") echo "selected"; ?> >+</option>
                        <option value="minus" <?php if($e->add_minus=="minus") echo "selected"; ?>>-</option>
                    </select>
                </td>
                <td >
                    <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( 
                        array( "name"        => "add_minus_to", 
                            "field"          => "add_minus_to",
                            "required"       => 1,
                            "class"          => "listen_math_calc_field", 
                            "id"             => $e->id,
                            "default"        => $e->add_minus_to
                        )
                    ); ?>
                </td>
                <td>
                <?php echo AUTOSUCCESS_UTIL::infusionSoftFieldsSelect2( 
                        array( "name"        => "store_result_here", 
                            "field"          => "store_result_here",
                            "required"       => 1,
                            "class"          => "listen_math_calc_field", 
                            "id"             => $e->id,
                            "default"        => $e->store_result_here
                        )
                    ); ?>
                </td>
                <td style="width:100px"> 
                    <?php 
                    $key = md5($e->id);
                    $url = site_url()."/wp-admin/admin-ajax.php?action=auto_success_ajax&c=math_calculator&task=receive&key=".$key;
                    ?>
                    <textarea style="width:100%; opacity:0; width:1px;opacity;position:absolute;" id="http-post-url-<?php echo $e->id;?>"><?php echo $url;?></textarea>
                    <button type="button" class="button button-secondary" onclick="copyToClipboardURL(this)" data-id="<?php echo $e->id;?>">Copy To Clipboard</button>


                    <button type="button" class="button-secondary" onclick="AutoSuccess.deleteMathCalculator(this);" data-id="<?php echo $e->id;?>">Delete</button>
                </td>
            </tr>
            <?php
        endforeach;
        ?>

    </tbody>

</table>