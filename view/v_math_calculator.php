
<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>

<style type="text/css">
.post-url-gen{
    padding:30px 10px;
}
.post-url-gen a{
    font-size:15px;
}
.date-formatter-list h4{
    font-size:13px;
    text-transform:uppercase;
    margin-top:-5px;
}
.date-formatter-list tr,
.date-formatter-list td,
.date-formatter-list th{
    font-size:12px;
}
.date-formatter-list th{
    font-weight:bolder;
}
</style>

<h2>Math Calculator</h2>

<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Add New </a></li>
        <li><a href="#tabs-2">List</a></li>
     </ul>
    <div id="tabs-1">
        <?php require_once("v_add_new_math_calc.php");?>
    </div>
    <div id="tabs-2">
        <div class="div-math-calculators-list"></div>
    </div>
  
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
    AutoSuccess.listMathCalculator();
});
</script>