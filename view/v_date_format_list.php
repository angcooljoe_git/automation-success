<?php 
function sel_infusionsoft($id,$default, $custom_fields, $fields, $field ){
     ob_start();
    ?>
    
    <select data-id="<?php echo $id;?>" data-field="<?php echo $field;?>" class="jq-custom-select listen-on-change"  style="width:100%">
       
        <option></option>

        <optgroup label="Standard Fields">
        <?php 
            foreach($fields as $efield):
                if($efield==$default) $selected = " selected ";
                else $selected = "  ";
                ?> 
                <option <?php echo $selected;?> label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                <?php 
            endforeach;
            ?>
        </optgroup>
        <optgroup label="Custom Fields">
        <?php 
        foreach($custom_fields as $efield):
            if("_".$efield["Name"]==$default) $selected = " selected ";
            else $selected = "  ";
            ?> 
            <option <?php echo $selected;?>  label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
            <?php 
        endforeach;
        ?>
        </optgroup>

    </select>
    <?php
    $select = ob_get_contents();
    ob_end_clean();

    return $select;
} // sel_infusionsoft
?>


<table class="wp-list-table widefat fixed striped posts">
	<thead>
	    <tr>
            <th>Name</th>
            <th style="width:200px">From Field</th>
            <th style="width:200px">To Field</th>
            <th style="width:180px;text-align:center">Format</th>
            <th></th>
         </tr>
    </thead>

    <tbody>
        <?php 
        foreach($list as $e):
            ?>
            <tr>
                <td style="position:relative">
                    <input style="width:100%; height:35px" type="text" class="form-control listen-on-change" data-field="name" data-id="<?php echo $e->id;?>" value="<?php echo $e->name;?>"/>
                </td>
                <td style="position:relative">
                    <?php echo sel_infusionsoft($e->id, $e->from_field, $custom_fields, $fields, "from_field"); ?>
                </td>
                <td style="position:relative">
                    <?php echo sel_infusionsoft($e->id, $e->to_field, $custom_fields, $fields, "to_field"); ?>
                </td>
                <td style="text-align:center; position:relative">
                     <select data-id="<?php echo $e->id;?>" data-field="format" style="width:100%" class="jq-custom-select listen-on-change" >
                        <option></option>
                        <?php 
                        foreach($date_formats as $edate):
                            if($edate==$e->format) $selected = "selected";
                            else $selected = "";
                        ?>
                            <option <?php echo $selected;?> value="<?php echo $edate;?>"><?php echo date( str_replace("at",'\a\t',$edate) );?></option>
                        <?php 
                        endforeach;
                        ?>
                    </select>
                </td>
                 
                <td style="position:relative; text-align:right ">
                    <?php 
                    $key = $e->key;
                    $url = site_url()."/wp-admin/admin-ajax.php?action=auto_success_ajax&c=dateformat&task=receive&key=".$key;
                    ?>
                    <textarea style="width:100%; opacity:0; width:1px;opacity;position:absolute;" id="http-post-url-<?php echo $e->id;?>"><?php echo $url;?></textarea>
                    <button type="button" class="button button-secondary" onclick="copyToClipboardURL(this)" data-id="<?php echo $e->id;?>">Copy To Clipboard</button>
                
                    <button type="button" class="button button-primary" data-id="<?php echo $e->id;?>" onclick="deleteDateFormatList(this)">Delete</button>
                </td>
            </tr>
            <?php 
        endforeach;
        ?>  
    </tbody>
</table>