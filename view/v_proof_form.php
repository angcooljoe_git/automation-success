<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>
<div id="poststuff" style="max-width:500px">
    <div class="postbox ">
        <button type="button" class="handlediv" aria-expanded="true">
        <span class="screen-reader-text">Toggle panel: Proof</span>
        <span class="toggle-indicator" aria-hidden="true"></span>
        </button>

        <h2 class="hndle ui-sortable-handle"><span> Edit proof</span>
        
        </h2>
       
        <div class="inside">
            
            <form id="frm-auto-success-proof" onsubmit="AutoSuccess.saveProof(this); return false;">
                <?php 
                if(!empty($proof_details->id)): 
                    ?>
                    <input type="hidden" name="id" value="<?php echo $proof_details->id;?>" />
                    <?php
                endif;
                ?>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col"> </div>
                        
                        <div class="col">

                            <?php 
                            if(isset($_REQUEST["is_new"]) ):
                            ?>
                                <div id="message" class="updated notice notice-success is-dismissible" style="margin:0px; margin-top:30px; margin-bottom:10px;">
                                    <p>New proof succesfully added</p>
                                    <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                                </div>
                            <?php 
                            endif;
                            ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">Proof Name: </div>
                        <div class="col">
                            <div class="position:relative"> 
                                <input  type="text"  name="proof_name" placeholder="Type proof name here ... " value="<?php echo $proof_details->proof_name;?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">Tag ID : </div>
                        <div class="col">
                            
                            
                            <div class="position:relative"> 
                                <input type="hidden" class="hdn-tag-id" name="tag_id" value="<?php echo $proof_details->tag_id;?>" />
                                <input autocomplete="off" type="text" class="txt-search-tag" name="tag_name" placeholder="Type tag name here ... " value="<?php echo $proof_details->tag_name;?>"/>
                                <div class="ci-dropdown-container"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col" style="vertical-align:middle">Message : </div>
                        <div class="col"> 
                            <textarea name="message" style="height:120px; width:300px"><?php echo $proof_details->message;?></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">Image : </div>
                        <div class="col"> 
                            <input type="text" class="media-input" name="image" value="<?php echo $proof_details->image;?>"/>
                            <button class="media-button button-secondary">Select image</button>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col"> </div>
                        <div class="col"> 
                                
                            <select name="before_notif_show">
                            <?php 
                            $seconds = array(0,5,10,15,20,25,30,35,40,45,50,55,60);
                            foreach($seconds as $esec):
                                if($esec == $proof_details->before_notif_show ) $selected = " selected ";
                                else $selected = "";
                                ?>
                                <option value="<?php echo $esec;?>" <?php echo $selected;?> ><?php echo $esec;?></option>
                                <?php
                            endforeach;
                            ?>
                            </select> seconds :  Time before the nofication to display
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col"></div>
                        <div class="col"> 
                            <select name="delay_next">
                            
                            <?php 
                            $seconds = array(0,5,10,15,20,25,30,35,40,45,50,55,60);
                            foreach($seconds as $esec):
                                if($esec == $proof_details->delay_next ) $selected = " selected ";
                                else $selected = "";
                                ?>
                                <option value="<?php echo $esec;?>" <?php echo $selected;?> ><?php echo $esec;?></option>
                                <?php
                            endforeach;
                            ?>
                            </select> 
                            seconds : Delay before showing next notification
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col"> </div>
                        <div class="col"> 
                            <button type="submit" class="button-primary">Save</button>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col"> </div>
                        <div class="col">  
                            
                            <div id="message" class="updated notice notice-success is-dismissible" style="margin:0px; margin-top:30px; margin-bottom:10px;">
                                <p>Copy this Code before the "<?php echo htmlentities("</body>");?>" end tag </p>
                                <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                            </div>

                            <?php 
                            ob_start();
                            ?><script type="text/javascript" src="<?php echo admin_url('admin-ajax.php'); ?>?action=auto_success_snippet&id=<?php echo $proof_details->id;?>"></script><?php
                            $snippet = ob_get_contents();
                            ob_end_clean();

                            ?>
                            <textarea style="width:100%; height:100px; padding:20px;"><?php echo htmlentities($snippet);?></textarea>
                        </div>
                    </div>

                </div>
            </form>

        </div> <!-- <div class="inside"> -->

    </div> <!-- .postbox -->

</div> <!-- #poststuff -->



<script type="text/javascript">
jQuery(document).ready(function(){
    var gk_media_init = function(selector, button_selector)  {
        var clicked_button = false;
    
        jQuery(selector).each(function (i, input) {
            var button = jQuery(input).next(button_selector);
            button.click(function (event) {
                event.preventDefault();
                var selected_img;
                clicked_button = jQuery(this);
    
                // check for media manager instance
                if(wp.media.frames.gk_frame) {
                    wp.media.frames.gk_frame.open();
                    return;
                }
                // configuration of the media manager new instance
                wp.media.frames.gk_frame = wp.media({
                    title: 'Select image',
                    multiple: false,
                    library: {
                        type: 'image'
                    },
                    button: {
                        text: 'Use selected image'
                    }
                });
    
                // Function used for the image selection and media manager closing
                var gk_media_set_image = function() {
                    var selection = wp.media.frames.gk_frame.state().get('selection');
    
                    // no selection
                    if (!selection) {
                        return;
                    }
    
                    // iterate through selected elements
                    selection.each(function(attachment) {
                        var url = attachment.attributes.url;
                        clicked_button.prev(selector).val(url);
                    });
                };
    
                // closing event for media manger
                wp.media.frames.gk_frame.on('close', gk_media_set_image);
                // image selection event
                wp.media.frames.gk_frame.on('select', gk_media_set_image);
                // showing media manager
                wp.media.frames.gk_frame.open();
            });
        });
    };
    gk_media_init('.media-input', '.media-button');
});
</script>

