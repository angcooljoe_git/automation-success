<div style="width:500px">

    <form class="" onsubmit="AutoSuccess.saveNewDateCalculator(this); return false;">

        <div class="form-group">
            <div class="row">
                <div class="col col-label" style="width:120px; text-align:left">Name </div>
                <div class="col">
                    <input type="text" class="form-control" name="name" />
                </div>
                
            </div>

        </div> <!-- .form-group -->

        <div class="form-group">
            <div class="row">
                <div class="col col-label" style="width:120px; text-align:left">Start with this date</div>
                <div class="col">
                    <select class="start_date jq-custom-select" name="start_date">
                        <option value="today">Today</option>
                        <option value="yesterday">Yesterday</option>
                        <option value="tomorrow">Tomorrow</option>
                    </select>
                </div>
            </div>
        </div> <!-- .form-group -->

        <div class="form-group">
            <div class="row">

                <div class="col col-label" style="width:50px">
                    Then    
                </div> <!-- .col -->

                <div class="col" style="width:100px; vertical-align:middle">
                    <select class="jq-custom-select add_minus listen-add-minus" style="width:100%" name="add_minus">
                        <option value="add">+ Add</option>
                        <option value="minus">- Minus</option>
                        <option value="custom">Custom</option>
                    </select>
                </div> <!-- .col -->

                <div class="col hide-on-custom"  style="width:100px">
                    <div class="with-label">
                        <input type="number" class="form-control" name="months" min="0"/>
                        <span class="label">Months</span>
                    </div>
                </div> <!-- .col -->

                <div class="col">
                    <div class="with-label hide-on-custom">
                        <input type="number" class="form-control" name="days" min="0"/>
                        <span class="label">Days</span>
                    </div>
                    <div class="custom" style="display:none">
                        <b>PHP Strtotime: </b>
                        <textarea  class="form-control textarea-custom" name="custom" style="width:200px; height:50px"></textarea>
                        <span class="sample-custom" style="font-weight:bold; display:block"></span>
                     </div>
                </div> <!-- .col -->

            </div> <!-- .row -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <div class="row">
                <div class="col" style="text-align:left; width:50%; ">
                    
                    <h4>Target Infusionsoft Field</h4>

                    <select class="jq-custom-select listen-on-change"  style="width:100%" name="target_infusionsoft_field">
                        <option></option>
                        <optgroup label="Standard Fields">
                        <?php 
                            foreach($fields as $efield):
                                if($efield==$default) $selected = " selected ";
                                else $selected = "  ";
                                ?> 
                                <option <?php echo $selected;?> label="Standard Infusionsoft Fields" value="<?php echo $efield;?>"><?php echo $efield;?></option>
                                <?php 
                            endforeach;
                            ?>
                        </optgroup>
                        <optgroup label="Custom Fields">
                        <?php 
                        foreach($custom_fields as $efield):
                            if("_".$efield["Name"]==$default) $selected = " selected ";
                            else $selected = "  ";
                            ?> 
                            <option <?php echo $selected;?>  label="Custom Fields" value="<?php echo "_".$efield["Name"];?>">_<?php echo $efield["Name"];?></option>
                            <?php 
                        endforeach;
                        ?>
                        </optgroup>

                    </select>

                </div> <!-- .col -->

                <div class="col" style="text-align:left">
                    <h4>Select Format</h4>
                    <select style="width:100%" class="jq-custom-select select-date-format" name="date_format">
                        <option></option>
                        <?php 
                        foreach($date_formats as $edate):
                            if($edate=="custom"): 
                                $date_value = "Custom";
                            else: 
                                $date_value = date( str_replace("at",'\a\t',$edate) );
                            endif;
                        ?>  
                        <option value="<?php echo $edate;?>"><?php echo $date_value;?></option>
                        <?php 
                        endforeach;
                        ?>
                    </select>
                    
                    <div class="custom-select-date-format" style="display:none; margin-top:20px;">
                        <b>Custom Date Format</b>
                        <input style="width:100%" type="text" class="form-control" name="custom_date_format"  />
                        
                    </div>

                </div>  <!-- .col -->

            </div> <!-- .row -->

        </div> <!-- form-group -->

        <div class="form-group">
            <div class="row">
                    <div class="col" style="text-align:center; padding:20px 0px">
                        <button type="submit" class="button-primary">Save</button>
                    </div>
            </div> <!-- .row -->
        </div> <!-- .form-group -->
    
    </form>

 
</div> <!-- width:500px -->