<?php require_once(WP_PLUGIN_DIR."/automation_success/assets/inc.php");?>
<div id="poststuff" style="max-width:500px">
    <div class="postbox ">
        <button type="button" class="handlediv" aria-expanded="true">
            <span class="screen-reader-text">Toggle panel: Settings </span>
            <span class="toggle-indicator" aria-hidden="true"></span></button>
            <h2 class="hndle ui-sortable-handle"><span>Settings</span></h2>
       
        <div class="inside">

            <form id="frm-settings" onsubmit="AutoSuccess.submitSettings(this); return false;">
                <div class="form-group">
                    <div class="row">
                        <div class="col">App Name :</div> 
                        <div class="col"><input type="text" class="" name="app_name" value="<?php echo get_option("_automation_success_app_name");?>"/> .infusionsoft.com </div>
                    </div>
                
                    <div class="row">
                        <div class="col">API Key : </div>
                        <div class="col"><input type="text" class="" name="api_key" value="<?php echo get_option("_automation_success_api_key");?>"/> </div>
                    </div>

                    <div class="row">
                        <div class="col"> </div>
                        <div class="col"> 
                            <button type="submit" class="button-primary">Save</button>
                        </div>
                    </div>
                
                </div>
            </form>

        </div> <!-- <div class="inside"> -->

    </div> <!-- .postbox -->

</div> <!-- #poststuff -->







