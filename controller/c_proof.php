<?php
class c_proof extends AUTOSUCCESS{
    
    public static function update_proof(){
        $tag_id   = esc_sql($_REQUEST["tag_id"]);
        $tag_name = esc_sql($_REQUEST["tag_name"]);
        $message  = esc_sql($_REQUEST["message"]);
        $image    = esc_sql($_REQUEST["image"]);
        $proof_name = esc_sql($_REQUEST["proof_name"]);
        $before_notif_show = esc_sql($_REQUEST["before_notif_show"]);
        $delay_next        = esc_sql($_REQUEST["delay_next"]);
 
        if($_REQUEST["id"]):
            $sql = "UPDATE automationsuccess_proofs 
            SET tag_id='$tag_id',
                tag_name='$tag_name',
                message='$message',
                image='$image',
                proof_name='$proof_name',
                before_notif_show ='$before_notif_show',
                delay_next = '$delay_next' 
            WHERE id = ".esc_sql($_REQUEST["id"])."    
            ";
            $response["redirect"] = 0;
            parent::wpdb()->query($sql);
            $id = $_REQUEST["id"];
        else: 
            $sql = "INSERT INTO automationsuccess_proofs(tag_id, tag_name, message,image,before_notif_show,delay_next,proof_name)
             VALUES('$tag_id','$tag_name','$message','$image','$before_notif_show','$delay_next','$proof_name')";
            $response["redirect"] = 1;
            parent::wpdb()->query($sql);
            $id = parent::wpdb()->insert_id;
        endif;
       

        $response["id"] = $id;
        echo json_encode($response);
        die();
    }  // update_proof

    public static function delete_proof(){
        $id = $_REQUEST["id"];
        parent::wpdb()->query("DELETE FROM automationsuccess_proofs WHERE id=".$id." LIMIT 1 ");
    }

} // c_main

?>