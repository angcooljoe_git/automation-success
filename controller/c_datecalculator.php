<?php
class c_datecalculator extends AUTOSUCCESS{
    public static function index(){
        $data = array();

        $data["custom_fields"] = M_CI_INFUSIONSOFT::get_custom_is_fields();
        $data["fields"]        = M_CI_INFUSIONSOFT::get_standard_is_fields();
        $data["date_formats"]  = M_CI_INFUSIONSOFT::get_date_format();

        auto_success_view("date_calculator", $data);
    } // index

    public static function save_new_datecalculator(){
    
        $start_date                = "'".esc_sql($_POST[start_date])."'";
        $add_minus                 = "'".esc_sql($_POST[add_minus])."'";
        $months                    = "'".esc_sql($_POST[months])."'";
        $days                      = "'".esc_sql($_POST[days])."'";
        $target_infusionsoft_field = "'".esc_sql($_POST[target_infusionsoft_field])."'";
        $date_format               = "'".esc_sql($_POST[date_format])."'";
        $custom                    = "'".esc_sql($_POST[custom])."'";
        $custom_date_format        = "'".esc_sql($_POST[custom_date_format])."'";
        $name                      = "'".esc_sql($_POST[name])."'";

        $sql = "INSERT INTO automationsuccess_datecalculator(start_date, add_minus, months, days, target_infusionsoft_field, date_format, custom, custom_date_format, name)
        VALUES($start_date, $add_minus, $months, $days, $target_infusionsoft_field, $date_format, $custom, $custom_date_format, $name ) ";
       
        parent::wpdb()->query($sql);

        die();
    } //save_new_datecalculator


    public static function list_datecalculator(){
        $data["custom_fields"] = M_CI_INFUSIONSOFT::get_custom_is_fields();
        $data["fields"]        = M_CI_INFUSIONSOFT::get_standard_is_fields();
        $data["date_formats"]  = M_CI_INFUSIONSOFT::get_date_format();

    
        $data["list"] = parent::wpdb()->get_results("SELECT * FROM automationsuccess_datecalculator ORDER BY `id` DESC ");
        auto_success_view("list_date_calculator", $data );
        die();
    } // llist_datecalculator

    public static function update(){
       
        $val   = "'".esc_sql($_GET["val"])."'";
        $id    = $_GET["id"];
        $field = $_GET["field"];
        parent::wpdb()->query("UPDATE automationsuccess_datecalculator SET $field=$val WHERE id=$id LIMIT 1 ");
    } // update

    public static function delete(){
        $id = $_POST["id"];
        parent::wpdb()->query("DELETE FROM automationsuccess_datecalculator  WHERE id=$id LIMIT 1 ");

    } // delete

    public static function receive(){
        $key = $_GET["key"];
        $cid = $_POST["contactId"];


        if(empty($cid)): 
            die();
        else: 
            $date_calculator = parent::wpdb()->get_results("SELECT * FROM automationsuccess_datecalculator WHERE md5(`id`)='$key' ");
            $date_calculator = $date_calculator[0];
            
            if($date_calculator->date_format == "custom"): 
                $date_calculator->date_format = $date_calculator->custom_date_format;
            endif;
            /*
            [start_date] => today
            [add_minus] => minus
            [months] => 3
            [days] => 4
            [target_infusionsoft_field] =>  Address1Type
            [date_format] => l F jS, Y, g:ia 
            
            */

            $target_infusionsoft_field = $date_calculator->target_infusionsoft_field;

            $start_Y = date("Y",strtotime($date_calculator->start_date) );
            $start_m = date("m",strtotime($date_calculator->start_date) );
            $start_d = date("d",strtotime($date_calculator->start_date) );
            $start_H = date("H",strtotime($date_calculator->start_date) );
            $start_i = date("i",strtotime($date_calculator->start_date) );
            $start_s = date("s",strtotime($date_calculator->start_date) );
            

            switch($date_calculator->add_minus):
                case "minus":
                    $new_date = date( $date_calculator->date_format ,mktime($start_H, 
                                    $start_i, 
                                    $start_s, 
                                    $start_m - $date_calculator->months, 
                                    $start_d - $date_calculator->days, 
                                    $start_Y
                                )
                        );
                break; # minus

                case "add":
                    $new_date = date( $date_calculator->date_format  ,mktime($start_H, 
                                $start_i, 
                                $start_s, 
                                $start_m + $date_calculator->months, 
                                $start_d + $date_calculator->days, 
                                $start_Y
                            )
                    );
                break; #add

                case "custom":
                    $new_date = date( $date_calculator->date_format, strtotime($date_calculator->custom) );
                 break; #custom
            endswitch;
            
          

            $start_date = date($date_calculator->date_format ,mktime($start_H, 
                                            $start_i, 
                                            $start_s, 
                                            $start_m, 
                                            $start_d, 
                                            $start_Y
                                        )
                                );

                                
            $update_fields[$target_infusionsoft_field] = $new_date; 
            parent::iSDK()->updateCon($cid, $update_fields);

            header('Content-Type: application/json');
            $response["success"] = 1;
            //$response[$to_field] = $new_value;
            echo json_encode( $response );
        endif; 
        die();
    } // receive

    public static function sample_custom(){
        $custom = $_GET["custom"];
        if(date("Y",strtotime($custom)) == "1970"): 
            echo "<span style='color:#b8132c'>Invalid format</span>";
        else:
            echo date("F d, Y",strtotime($custom));
        endif;

        die();
    }// sample_custom

} // datecalculator