<?php
class c_math_calculator extends AUTOSUCCESS{
    
    public static function index(){
        $data = array();
        auto_success_view("math_calculator",$data);
    } // index 

    public static function save_new_mathcalculator(){

        $name              = "'".esc_sql($_POST[name])."'";
        $add_minus_from    = "'".esc_sql($_POST[add_minus_from])."'";
        $add_minus         = "'".esc_sql($_POST[add_minus])."'";
        $add_minus_to      = "'".esc_sql($_POST[add_minus_to])."'";
        $store_result_here = "'".esc_sql($_POST[store_result_here])."'";

        parent::wpdb()->query("INSERT INTO 
                automationsuccess_mathcalculator(name, add_minus_from, add_minus, add_minus_to, store_result_here)
                VALUES($name, $add_minus_from, $add_minus, $add_minus_to, $store_result_here )
            ");
        die();
    } // save_new_mathcalculator

    public static function list_mathcalculator(){
        $data["list"] = parent::wpdb()->get_results("SELECT * FROM automationsuccess_mathcalculator ORDER BY `id` DESC ");
        auto_success_view("list_math_calculator", $data );
        die();
    } //list_mathcalculator

    public static function update(){
        $field = $_POST["field"];
        $val   = "'".esc_sql($_POST["val"])."'";
        $id    = $_POST["id"];
        $sql = "UPDATE automationsuccess_mathcalculator SET $field = $val WHERE id=$id LIMIT 1 ";
        parent::wpdb()->query($sql);

        die();
    } // update update

    public static function delete(){
        $id = $_POST["id"];
        parent::wpdb()->query("DELETE FROM automationsuccess_mathcalculator WHERE id =".$id." LIMIT 1 " );
    } // delete

    public static function receive(){
        $key = $_GET["key"];
        $cid = $_POST["contactId"];
 
        if(empty($cid)): 
            die();
        else: 
            $math_calculator = parent::wpdb()->get_results("SELECT * FROM automationsuccess_mathcalculator WHERE md5(`id`)='$key' ");
            $math_calculator = $math_calculator[0];
             
            $table = 'Contact';
            $limit = 1000;
            $page = 0;
            $queryData = array('Id' => $cid);
            $selectedFields[] = $math_calculator->add_minus_from;
            $selectedFields[] = "Id";
            $selectedFields[] = $math_calculator->add_minus_to;
             
         
            $contact   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
            $contact   = $contact[0];
            
            if( $math_calculator->add_minus == "add"): 
                $result = $contact[ $math_calculator->add_minus_from ] + $contact[ $math_calculator->add_minus_to ];
            else: 
                $result = $contact[ $math_calculator->add_minus_to ] - $contact[ $math_calculator->add_minus_from ];
            endif;
                                
            $update_fields[$math_calculator->store_result_here] = $result; 
            parent::iSDK()->updateCon($cid, $update_fields);

            header('Content-Type: application/json');
            $response["success"] = 1;
            $response[$to_field] = $new_value;
            echo json_encode( $response );
            
        endif; 
        die();
    } // receive

}