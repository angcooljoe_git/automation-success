<?php
class c_dateformat extends AUTOSUCCESS{
    public static function date_format(){
        $data = array();

        $data["custom_fields"] = M_CI_INFUSIONSOFT::get_custom_is_fields();
        $data["fields"]        = M_CI_INFUSIONSOFT::get_standard_is_fields();
        $data["date_formats"]  = M_CI_INFUSIONSOFT::get_date_format();
        # end standard date formats

        auto_success_view("date_format", $data);
    } // date_format

    public static function list(){
        
        $data["custom_fields"] = M_CI_INFUSIONSOFT::get_custom_is_fields();
        $data["fields"]        = M_CI_INFUSIONSOFT::get_standard_is_fields();
        $data["list"]          = parent::wpdb()->get_results("SELECT * FROM automationsuccess_dateformat ");

        # standard date formats
        $data["date_formats"]  = M_CI_INFUSIONSOFT::get_date_format();
        # end standard date formats

        auto_success_view("date_format_list", $data);
        die();
    } //

    public static function generate_http_post_url(){

        $key        = "'".md5(mt_rand().mt_rand())."'";
        $from_field = "'".esc_sql($_POST["update_from_field"])."'";
        $to_field   = "'".esc_sql($_POST["update_to_field"])."'";
        $format     = "'".esc_sql($_POST["date_format"])."'";
        $name       = "'".esc_sql($_POST["name"])."'";
        
        $sql        = "INSERT INTO automationsuccess_dateformat(`key`,from_field,to_field,`format`,`name`) 
                            VALUES($key, $from_field, $to_field, $format, $name)
                        ";
       
 
        parent::wpdb()->query($sql);
        /*
        add_option("autosuccess_".$key, $_POST );

        $url = site_url()."/wp-admin/admin-ajax.php?action=auto_success_ajax&c=dateformat&task=receive&key=".$key;

        ?> 
        <div style="text-align:center">
            
            <textarea class="hidden-generated-url" style="
            width:100%;
            height:100px;
            padding:20px 10px;
            "><?php echo $url;?></textarea>

            <br/>
            <button type="button" class="button button-secondary" onclick="copyToClipboardURL()">Copy to Clipboard</button>
        </div>
        <?php 
        */

        die();
    } // generate_http_post_url


    public static function test_contact(){
        $cid = $_GET["cid"];

        $table = 'Contact';
        $limit = 1000;
        $page = 0;
        $queryData = array('Id' => $cid);
        $selectedFields[] = "Password";
        $selectedFields[] = "_FreeClassDate";
        $selectedFields[] = "Id";
        $selectedFields[] = "_FreeClassDateHuman";
        $selectedFields[] = "Phone1";
        
       

        $orderBy = 'Id';
        $ascending = true;

        $contact   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
        #$contact   = $contact[0];

        echo "<pre>";
            print_r($contact); 
            print_r($selectedFields);
        echo "</pre>";
    }

    public static function receive(){
        $key = $_GET["key"];
        $cid = $_POST["contactId"];

        if(empty($cid)): 
            die();
        else: 
            $dateformat = parent::wpdb()->get_results("SELECT * FROM automationsuccess_dateformat WHERE `key`='$key' ");
            $dateformat = $dateformat[0];
 

            $from_field  = $dateformat->from_field;
            $to_field    = $dateformat->to_field;
            $date_format = str_replace("at",'\a\t',$dateformat->format);

            # get contact data
            $table = 'Contact';
            $limit = 1000;
            $page = 0;
            $queryData = array('Id' => $cid);
            $selectedFields[] = $from_field;
            $selectedFields[] = "Id";
            $selectedFields[] = $to_field;

            $orderBy = 'Id';
            $ascending = true;

            $contact   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
            $contact   = $contact[0];


            ## 
            $current_value = $contact[$from_field];
            $new_value     = date($date_format, strtotime($current_value) );

            $update_fields[$to_field] = $new_value; 
            parent::iSDK()->updateCon($cid, $update_fields);

            header('Content-Type: application/json');
            $response["success"] = 1;
            $response[$to_field] = $new_value;
            echo json_encode( $response );
        endif; 
        die();
    } // receive

    public static function update_date_format(){
 
        $field = $_POST["field"];
        $val   = "'".esc_sql($_POST["val"])."'";
        $id    = $_POST["id"];
        $sql = "UPDATE automationsuccess_dateformat SET $field = $val WHERE id=$id LIMIT 1 ";
        parent::wpdb()->query($sql);

    } //update_date_format 

    public static function delete_dateformat_list(){
        $id = $_POST["id"];
        parent::wpdb()->query("DELETE FROM automationsuccess_dateformat WHERE id=".$id." LIMIT 1 ");
    }
} // c_main extends