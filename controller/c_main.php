<?php
class c_main extends AUTOSUCCESS{
    public static function index(){
       

        auto_success_view("welcome");
    }

    public static function settings(){
        $data = $_REQUEST;
        auto_success_view("settings", $data);
    } // settings

    public static function proof(){
        $data = $_REQUEST;
        $data["proofs"] = parent::wpdb()->get_results("SELECT * FROM automationsuccess_proofs");
        auto_success_view("proof", $data);
    } // proof

    public static function proof_form(){
        $data = $_REQUEST;
        $proof_details         = parent::wpdb()->get_results("SELECT * FROM automationsuccess_proofs WHERE id=".$data["id"]);
        $data["proof_details"] = $proof_details[0];
        auto_success_view("proof_form", $data);
    }

    public static function get_infusionsoft_group(){
        
        $table = 'ContactGroup';
        $limit = 1000;
        $page = 0;
        $queryData = array('GroupName' => $_REQUEST["skey"].'%');
        $selectedFields = array('Id','GroupName');
        $orderBy = 'Id';
        $ascending = true;

        $tags   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
        $groups = array();
        foreach($tags as $etag):
            $groups[$etag["Id"]] = $etag["GroupName"];
        endforeach;
        $data["groups"] = $groups;

        auto_success_view("search_group_dropdown",$data);
        die();
    } // get_infusionsoft_group
    
    public static function date_format(){
        $data = array();

        $table = 'DataFormField';
        $limit = 1000;
        $page = 0;
        $queryData = array('FormId' => "-1" );
        $selectedFields = array("Id","Name");
        $orderBy = '';
        $ascending = true;

        $custom_fields = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
        
        $data["custom_fields"] = $custom_fields;
        $data["fields"] = M_CI_INFUSIONSOFT::get_standard_is_fields();

        # standard date formats
        $date_formats   = array();
        $date_formats[] = "l F jS at g:ia "; 
        $date_formats[] = "l F jS, Y, g:ia ";
        $date_formats[] = "l F j, Y"; 
        $date_formats[] = "l, F jS"; 
        $date_formats[] = "F j, Y g:ia ";
        $date_formats[] = "m/d/y g:ia ";
        $date_formats[] = "m/d/y";
        $date_formats[] = "d/m/y g:ia ";
        $date_formats[] = "d/m/y";
        $date_formats[] = "F Y";
        $date_formats[] = "F";
        $date_formats[] = "l";
        $data["date_formats"] = $date_formats;
        # end standard date formats

        auto_success_view("date_format", $data);
    } // date_format

    public static function generate_http_post_url(){
         
        $key = md5(mt_rand().mt_rand());
        add_option("autosuccess_".$key, $_POST );

        $url = site_url()."/wp-admin/admin-ajax.php?action=auto_success_ajax&c=main&task=receive&key=".$key;

        ?> 
        <div style="text-align:center">
            
            <textarea class="hidden-generated-url" style="
            width:100%;
            height:100px;
            padding:20px 10px;
            "><?php echo $url;?></textarea>

            <br/>
            <button type="button" class="button button-secondary" onclick="copyToClipboardURL()">Copy to Clipboard</button>
        </div>
        <?php 

        die();
    } // generate_http_post_url


    public static function test_contact(){
        $cid = $_GET["cid"];

        $table = 'Contact';
        $limit = 1000;
        $page = 0;
        $queryData = array('Id' => $cid);
        $selectedFields[] = "_FreeClassDate";
        $selectedFields[] = "Id";
        $selectedFields[] = "_FreeClassDateHuman";

        $orderBy = 'Id';
        $ascending = true;

        $contact   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
        $contact   = $contact[0];

        echo "<pre>";
            print_r($contact);
        echo "</pre>";
    }

    public static function receive(){
        $key = $_GET["key"];
        $opt = get_option("autosuccess_".$key);
        $cid = $_POST["cid"];

        if(empty($cid)): 
            die();
        else: 
            
            $from_field  = $opt["update_from_field"];
            $to_field    = $opt["update_to_field"];
            $date_format = str_replace("at",'\a\t',$opt["date_format"]);


            # get contact data
            $table = 'Contact';
            $limit = 1000;
            $page = 0;
            $queryData = array('Id' => $cid);
            $selectedFields[] = $from_field;
            $selectedFields[] = "Id";
            $selectedFields[] = $to_field;

            $orderBy = 'Id';
            $ascending = true;

            $contact   = parent::iSDK()->dsQuery($table, $limit, $page, $queryData, $selectedFields); 
            $contact   = $contact[0];


            ## 
            $current_value = $contact[$from_field];
            $new_value     = date($date_format, strtotime($current_value) );

            $update_fields[$to_field] = $new_value; 
            parent::iSDK()->updateCon($cid, $update_fields);

            header('Content-Type: application/json');
            $response["success"] = 1;
            $response[$to_field] = $new_value;
            echo json_encode( $response );
        endif; 
        die();
    } // receive

} // c_main

?>